/* ===========================================================================
Created:	2015/08/15 - https://github.com/yes4me/
Author:		Thomas Nguyen - thomas_ejob@hotmail.com
Purpose:	My library for Excel files
Requires: FROM: Apache POI - https://poi.apache.org/download.html
	poi-3.12-20150511.jar
	poi-ooxml-3.12-20150511.jar
	poi-ooxml-schemas-3.12-20150511.jar
	xmlbeans-2.6.0.jar

You can also use JExcelApi - http://jexcelapi.sourceforge.net/
=========================================================================== */

package lib;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class MyExcel {
	public boolean read(String file) {
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		Workbook wb;
		try {
			wb = WorkbookFactory.create(fis);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		Sheet sheet			= wb.getSheet("Sheet1");
		for (Row row : sheet) {
			System.out.println(row.getCell(0).getStringCellValue()+" : "+row.getCell(1).getStringCellValue());
		}
		return true;
	}
	/*
	private static int findRow(HSSFSheet sheet, String cellContent) {
	    for (Row row : sheet) {
	        for (Cell cell : row) {
	            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
	                if (cell.getRichStringCellValue().getString().trim().equals(cellContent)) {
	                    return row.getRowNum();
	                }
	            }
	        }
	    }
	    return 0;
	}
	*/
	public static void main(String[] args) {
		MyExcel myExcel = new MyExcel();
		myExcel.read("test.xlsx");
	}
}