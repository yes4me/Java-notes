/* ===========================================================================
Created:	2015/08/10 - https://github.com/yes4me/
Author:		Thomas Nguyen - thomas_ejob@hotmail.com
Purpose:	My library
=========================================================================== */

package lib;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

public class MyFile {
	public static boolean fileExist(String filePath) {
		File file = new File(filePath);
		return ( file.exists() && !file.isDirectory() );
	}
	public static String[] convertString(Set<Object> numbers)
	{
		String[] result = new String[ numbers.size() ];
		return numbers.toArray(result);
	}

	//========================================================
	// BASIC TEXT FILES
	//========================================================
	public static String readTextFile(String filePath) {
		if (!fileExist(filePath))
			return "";

		StringBuffer fileContent = new StringBuffer("");
		BufferedReader br = null;
		try
		{
			String sCurrentLine;
			br = new BufferedReader(new FileReader(filePath));
			while ((sCurrentLine = br.readLine()) != null)
			{
				fileContent.append(sCurrentLine);
				fileContent.append("\n");
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (br != null)
					br.close();
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}
		}
		String result	= fileContent.toString();
		result			= result.replaceAll("(\\s)$", "");
		return result;
	}
	public static boolean writeTextFile(String filePath, String data) {
		if (!fileExist(filePath))
			return false;

		BufferedWriter output = null;
		try {
			File file = new File(filePath);
			output = new BufferedWriter(new FileWriter(file));
			output.write(data);

			//flush the stream
			output.flush();

			//close the stream
			output.close();

			return true;
		} catch ( IOException e ) {
			e.printStackTrace();
		}
		return false;
	}

	//========================================================
	// BASIC PROPERTIES (TEXT) FILES (extension: *.properties)
	//========================================================

	public static String[] getPropertiesKeys(String filePath)
	{
		if (!fileExist(filePath))
			return null;

		Set<Object> returnValues= null;
		Properties properties	= new Properties();
		InputStream input		= null;
		try {
			input	= new FileInputStream(filePath);

			//load a properties file
			properties.load(input);
			returnValues = properties.keySet();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return convertString(returnValues);
	}

	public static String[] readProperties(String filePath, String... keys)
	{
		if (!fileExist(filePath))
			return null;

		ArrayList<String> returnValues = new ArrayList<String>();
		Properties properties	= new Properties();
		InputStream input		= null;
		try {
			input	= new FileInputStream(filePath);

			//load a properties file
			properties.load(input);

			//get the property value and print it out
			for (String key : keys)
				returnValues.add( properties.getProperty(key) );
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return MyCollection.convertString(returnValues);
	}
	public static boolean updateProperties(String filePath, String key, String value)
	{
		boolean returnValue = false;
		if (!fileExist(filePath))
			return returnValue;

		Properties properties	= new Properties();
		InputStream input		= null;
		OutputStream output		= null;
		try {
			input	= new FileInputStream(filePath);

			//load a properties file (if you don't load, the property file get overwritten)
			properties.load(input);

			output	= new FileOutputStream(filePath);

			//set a properties file
			properties.setProperty(key, value);

			//save properties to project root folder
			properties.store(output, null);

			returnValue = true;
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return returnValue;
	}

	//========================================================
	// OTHERS
	//========================================================

	public static Reader reader(String filePath) {
		Reader reader= null;
		try {
			reader = new FileReader(filePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return reader;
	}

	//Read a file content and write it to a new file in reverse order.( reverse line 1-10 to line 10-1)
	public static void main(String arg[])
	{
		String readTxt = readTextFile("test.txt");
		readTxt = MyPrimitive.reverseChar(readTxt);
		writeTextFile("test.txt", readTxt);
		System.out.println("##"+ readTxt +"##");

		boolean b= updateProperties("test_config.properties", "dbuser","thomas");
		System.out.println("b="+ b);
		String[] result = readProperties("test_config.properties", "database", "dbuser");
		for (int i=0; i<result.length; i++)
		{
			System.out.println("##"+ result[i] +"##");
		}
	}
}