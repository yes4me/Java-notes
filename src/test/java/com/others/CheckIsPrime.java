package com.others;

public class CheckIsPrime {
	public static boolean isPrime(int number) {
		if (number<1) return false;
		if (number<4) return true;
		//check if n is a multiple of 2
		if (number%2==0) return false;

		//if not, then just check the odd numbers
		int maxPossibleValue = (int)Math.sqrt(number);
		for(int i=3; i<=maxPossibleValue; i+=2)
		{
			if(number%i==0)
				return false;
		}
		return true;
	}

	public static void main(String[] args) {
		for (int i=1; i<30; i++)
		{
			boolean b = isPrime(i);
			if (b==true) System.out.println(i);
		}
	}
}
