/*
 *  http://www.programcreek.com/2012/12/leetcode-regular-expression-matching-in-java/
 *  https://leetcode.com/discuss/9405/the-shortest-ac-code
 */


package com.others;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Solution with regular expression used
public class Glob {
	private static String[] convertList(List<String> string)
	{
		int size = string.size();
		String[] result = new String[size];
		for (int i=0; i<size; i++) {
			result[i] = string.get(i);
		}
		return result;
	}
	
	private static String cleanPattern(String pattern) {
		String patternClean = pattern;
		while (true)
		{
			patternClean = pattern.replace("**", "*");
			patternClean = pattern.replace("??", "?");
			if (patternClean!=pattern)
				pattern = patternClean;
			else
				break;
		}
		return pattern;
	}
	public static String[] match(String pattern, String[] filenames) {
		if (pattern.isEmpty())
			return filenames;

		//clean the pattern from duplicate *
		pattern = cleanPattern(pattern);
		List<String> result = new ArrayList<String>();
		for (int i=0; i<filenames.length; i++)
		{
			String tmp = matchOneString(pattern, filenames[i]);
			if (!tmp.equals(""))
				result.add(tmp);
		}
		return convertList(result);
	}
	
	
	private static String replaceQuestion(String pattern, String filename) {
		StringBuilder returnValue = new StringBuilder(filename);
		for (int i=0; i<pattern.length(); i++)
		{
			if (pattern.charAt(i) == '?')
				returnValue.setCharAt(i, '?');
		}
		return returnValue.toString();
	}
	private static boolean myStartsWith(String pattern, String filename) {
		filename = replaceQuestion(pattern, filename); 
		return filename.startsWith(pattern);

		/*
		boolean questionMark = false;
		for (int i=0; i<pattern.length(); i++)
		{
			if ( pattern.charAt(i) == '?')
			{
				questionMark = true;
			}
		}
		
		return filename.startsWith(pattern);
		*/
	}
	private static boolean myEndsWith(String pattern, String filename) {
		filename = replaceQuestion(pattern, filename); 
		return filename.endsWith(pattern);
	}
	private static int myIndexOf(String pattern, String filename) {
		for (int i=0; i<filename.length(); i++)
		{
			String tmp = filename.substring(i);
			tmp = replaceQuestion(pattern, tmp);
			int index = tmp.indexOf(pattern);
			if (index!=-1)
				return index;
		}
		return -1;
	}
	
	//Without using any regular expression, split the pattern based on *, and check for matches 
	public static String matchOneString(String pattern, String filename)
	{
		String text = filename;
		if (pattern.isEmpty())
			return filename;

		char firstPattern		= pattern.charAt(0);
		char lastPattern		= pattern.charAt(pattern.length()-1);

		//Remove the first and last '*' for the split
		if (firstPattern == '*')
			pattern = pattern.substring(1);
		if (lastPattern == '*')
			pattern = pattern.substring(0, pattern.length()-1);
		List<String> patterns = new ArrayList<String>( Arrays.asList(pattern.split("\\*")) );

		//Check for first *
		if (firstPattern != '*')
		{
			if ( myStartsWith(patterns.get(0), text) )
			{
				text = text.substring(patterns.get(0).length());
				patterns.remove(0);
			}
			else return "";
		}

		//Check for last *
		if (lastPattern != '*')
		{
			if ( myEndsWith(patterns.get(patterns.size()-1), text) )
			{
				int filenameLength = text.length();
				int patternsLength = patterns.get(patterns.size()-1).length();
				text = text.substring(0, filenameLength - patternsLength);
				patterns.remove(patterns.size()-1);
			}
			else return "";
		}
		
		if (patterns.size()==0)
			return filename;

		//check for inside *
		for (String part : patterns)
		{
			int index = myIndexOf(part, text);
			if (index==-1)
				return "";
			text = text.substring(index+part.length()); 
		}
	
		return filename;
	}
	
	public static boolean arrayEquals(String[] array1, String[] array2) {
		if (array1.length != array2.length) {
			return false;
		}
		for (int i = 0;i < array1.length; i++) {
			if (!array1[i].equals(array2[i])) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		String[] tmp = match("?abc*", new String[] { "abcd", "dabc", "abc", "efabc", "eadd" });
		for (String tmp2 : tmp)
			System.out.println(tmp2);

		System.out.println(arrayEquals(new String[] { "abcd", "dabc", "abc" }, 
				match("?abc*", new String[] { "abcd", "dabc", "abc", "efabc", "eadd" })));
		System.out.println(arrayEquals(new String[] { "abcd", "dabc", "abc" }, 
				match("?a**c*", new String[] { "abcd", "dabc", "abc", "efabc", "eadd" })));
	}
}