package com.others;

public class Test {
	public static void main(String[] args) {
		String text = "abc def qwe";

		String[] a = text.split(" ", 2);
		for (int i=0; i<a.length; i++)
			System.out.println("##"+ a[i] +"##");

		String b = text.substring(0, text.indexOf(" "));
		String c = text.substring(text.indexOf(" ")+1, text.length());
		System.out.println(b +"=="+ c);

		String tmp = "12 345";
		System.out.println("==>"+ Long.parseLong(tmp) +"<==");
	}
}
