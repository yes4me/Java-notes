package com.others;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import lib.MyConsole;

public class HashMapStudent {
	private Map<Integer, Student> studentsMap = new HashMap<Integer, Student>();
	private static int studentCounter=0;

	private void addStudent()
	{
		System.out.print("student name: ");
		String name	 = MyConsole.consoleInputString();
		System.out.print("student email: ");
		String email = MyConsole.consoleInputString();
		System.out.print("student phone: ");
		int phone	 = MyConsole.consoleInputInt();

		studentsMap.put(studentCounter, new Student(name, email, phone) );
		studentCounter++;
	}
	private void displayAllStudents()
	{
		Set<Integer> keys		= studentsMap.keySet();
		Iterator<Integer> it	= keys.iterator();
		while (it.hasNext())
		{
			Integer key		= it.next();
			Student student = studentsMap.get(key);
			student.displayInfo();
			System.out.println("");
		}
	}

	//print id comment name
	public static void main(String[] args) {
		HashMapStudent hasMapStudent = new HashMapStudent();
		while (true)
		{
			System.out.println("*** PICK YOUR CHOICE ***");
			System.out.println("0: add a student");
			System.out.println("1: display ALL students");
			System.out.println("OTHER: EXIT");
			System.out.print("Your choice:");

			int menu = MyConsole.consoleInputInt();
			switch(menu)
			{
				case 0:
					hasMapStudent.addStudent();
					break;
				case 1:
					hasMapStudent.displayAllStudents();
					break;
				default:
					break;
			}
			if (menu>1)
				break;
		}
		MyConsole.close();
		System.out.print("DONE");
	}
}