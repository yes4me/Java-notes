package com.others;

public class Student {
	private String name		= "";
	private String email	= "";
	private int phone 		= 0;

	//Select all the variables, right click > Source > Generate Getters and Setters  
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}

	Student(String name, String email, int phone)
	{
		this.name = name;
		this.email = email;
		this.phone = phone;
	}
	public void displayInfo()
	{
		System.out.println("student name: "+ name);
		System.out.println("student email: "+ email);
		System.out.println("student phone: "+ phone);
	}
}
