/* ===========================================================================
Created:	2015/08/29 - https://github.com/yes4me/
Author:		Thomas Nguyen - thomas_ejob@hotmail.com
Purpose:	connect to DB
Require:	need mysql-connector-java-5.1.22-bin.jar from
			dev.mysql.com/downloads/connector/j/
=========================================================================== */

package com.interview.basic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DemoMySQL {
	private static String DBName	= "zillow";
	private static String userName	= "root";
	private static String password	= "MySQL";

	public boolean checkDriver() {
		//Check if the driver is installed
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public Connection getConnection(String DBName, String userName, String password) {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+ DBName, userName, password);
		} catch (SQLException e) {
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
		}
		return connection;
	}
	public boolean closeConnection(Connection conn) {
		if (conn != null)
		{
			try
			{
				conn.close();
			}
			catch (SQLException e)
			{
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		DemoMySQL demoSQL = new DemoMySQL();
		if ( !demoSQL.checkDriver() )
			System.out.println("MySQL JDBC Driver NOT found in Java Path");

		//Connect to the DB
		Connection conn = demoSQL.getConnection(DBName, userName, password);
		if (conn == null)
			System.out.println("Failed to make connection!");

		try {
			//Make a statement
			Statement s = conn.createStatement();

			//int rc=s.executeUpdate ("Update student set email='abcded@gmail.com' where id=3");
			//System.out.println(rc+" records are updated.");

			ResultSet rs= s.executeQuery("SELECT * FROM employee");
			while (rs.next())
			{
				int id			= rs.getInt("id");
				String name		= rs.getString("name");
				int salary		= rs.getInt("salary");
				String department	= rs.getString("department");
				System.out.println ("id="+ id +", name =" + name +", salary=" + salary +", department="+ department);
			}
			rs.close();
			s.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		demoSQL.closeConnection(conn);
	}
}