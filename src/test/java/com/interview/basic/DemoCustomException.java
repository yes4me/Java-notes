package com.interview.basic;

public class DemoCustomException {
	//MyException can also be saved in its own file 
	public static class MyException extends Exception {
	    public MyException(String message) {
	        super(message);
	    }
	}

	public static void main(String[] args) throws MyException {
		throw new MyException("Hello world");
	}
}