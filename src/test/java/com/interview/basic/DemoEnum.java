package com.interview.basic;

public class DemoEnum {
	public enum Days {
		BUTTON1,
		BUTTON2,
		BUTTON3
	}
	public static void main(String[] args) {
		Days currentDay = Days.valueOf("BUTTON"+ 3);
		switch (currentDay) {
			case BUTTON1 : System.out.println("button1"); break;
			case BUTTON2 : System.out.println("button2"); break;
			case BUTTON3 : System.out.println("button3"); break;
		}
	}
}