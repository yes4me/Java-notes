/*
Installation steps:
 - in build.gradle, add dependencies: testCompile 'log4j:log4j:1.2.17'
 - go to http://logging.apache.org/log4j/1.2/download.html, and download the ZIP file
 - in Eclipse, right click on project,  property, java build path, add external jar file log4j-1.2.17.jar
 - in src/test/java, add the configuration file for log4j: either log4j.property or log4j.xml

SOURCE: http://salmanawan.com/hello-world-%E2%80%93-log4j-tutorial/
		http://www.mkyong.com/logging/log4j-log4j-properties-examples/
		https://logging.apache.org/log4j/2.0/faq.html
*/

package com.interview.basic;

import org.apache.log4j.Logger;

public class DemoLog4J {
	private static final Logger logger = Logger.getLogger( DemoLog4J.class );

	public static void main(String[] args) {
		logger.trace("Sample trace message");
		logger.debug("Sample debug message");
		logger.info("Sample info message");
		logger.warn("Sample warn message");
		logger.error("Sample error message");
		logger.fatal("Sample fatal message");

		System.out.println("this is the main");
	}
}