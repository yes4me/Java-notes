# =========================================================
# Created:	2015/10/01
# Author:		Thomas Nguyen - thomas_ejob@hotmail.com
# Pick a Database
# =========================================================

SHOW DATABASES;				#show all databases available
USE zillow;					#select a database
SELECT DATABASE();			#show database used

# =========================================================
# Create table: Country
# =========================================================

SHOW TABLES;				#show all tables in the database
CREATE TABLE Country
(
	id int PRIMARY KEY,
	name varchar(255) NOT NULL UNIQUE
);
DROP TABLE Country;
COMMIT;						#MySQL has autohotkey

CREATE TABLE Country
(
	id int,
	name varchar(255) NOT NULL,
    CONSTRAINT pk_id PRIMARY KEY(id),
    CONSTRAINT uc_name UNIQUE(name)
);
DESC Country;
DROP TABLE Country;

# =========================================================
# Create table: employee
# =========================================================

CREATE TABLE employee
(
	id int PRIMARY KEY,
	name varchar(255) NOT NULL UNIQUE,
    salary int CHECK(salary>0),				#MySQL does not support SQL check constraints
    Country_id int NOT NULL REFERENCES Country,
);
DROP TABLE Employee;
COMMIT;						#MySQL has autohotkey

CREATE TABLE Employee
(
	id int,
	name varchar(255) NOT NULL,
    salary int,
    Country_id int NOT NULL,
    CONSTRAINT pk_id PRIMARY KEY(id),
    CONSTRAINT uc_name UNIQUE(name),
    CONSTRAINT ck_salary CHECK(salary>0),	#MySQL does not support SQL check constraints
    CONSTRAINT ref_id FOREIGN KEY (Country_id) REFERENCES Country(id)
);
DESC Employee;
DROP TABLE Employee;

# =========================================================
# Do some insert/update
# =========================================================

INSERT INTO Country(id, name) values (0, 'USA');
INSERT INTO Country(id, name) values (1, 'France');
INSERT INTO Country(id, name) values (2, 'Canada');
SELECT * FROM Country;

INSERT INTO Employee(id, name, salary, Country_id) values (0, 'name0', 0, 0);
INSERT INTO Employee(id, name, salary, Country_id) values (1, 'name1', 1000, 0);
INSERT INTO Employee(id, name, salary, Country_id) values (2, 'name2', 2000, 1);
INSERT INTO Employee(id, name, salary, Country_id) values (3, 'name3', 3000, 0), (4, 'name4', 4000, 2), (5, 'name5', 5000, 1);
SELECT * FROM Employee;
DELETE FROM Employee;
DELETE FROM Employee WHERE id=5;

UPDATE Employee SET Country_id=2 WHERE id<=1;

# =========================================================
# Do some queries
# =========================================================

SELECT * FROM Employee;
SELECT * FROM Employee AS WORKERS;
SELECT * FROM Employee ORDER BY name DESC;
SELECT * FROM Employee WHERE id IN(1,2);
SELECT count(*), max(salary), avg(salary), min(salary) FROM Employee WHERE name BETWEEN 'name2' AND 'name4';
SELECT count(*), max(salary), avg(salary), min(salary) FROM Employee WHERE name LIKE 'n_me%' AND salary>=3000;
SELECT Country_id, ROUND(AVG(salary),2) FROM Employee GROUP BY Country_id HAVING SUM(salary)>=3000;

SELECT * FROM Employee INNER JOIN Country ON Employee.id=Country.id;	#JOIN == INNER JOIN
SELECT * FROM Employee LEFT JOIN Country ON Employee.id=Country.id;		#LEFT JOIN == LEFT OUTER JOIN
SELECT * FROM Employee RIGHT JOIN Country ON Employee.id=Country.id;	#RIGHT JOIN == RIGHT OUTER JOIN

SELECT * FROM Employee LEFT JOIN Country ON Employee.id=Country.id
UNION
SELECT * FROM Employee RIGHT JOIN Country ON Employee.id=Country.id;	#This is the FULL OUTER JOIN

# =========================================================
# OTHERS
# =========================================================

ALTER TABLE Employee ADD COLUMN department int DEFAULT 1;				#Null value by default

CREATE INDEX IX_Employee_salary ON Employee (salary);
DROP INDEX IX_Employee_salary ON Employee;