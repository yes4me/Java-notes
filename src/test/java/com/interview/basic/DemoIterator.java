package com.interview.basic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DemoIterator {
	public static void main(String[] args) {
		Set<Integer> set = new HashSet<Integer>();
		set.add(1);
		set.add(2);
		set.add(3);
		Iterator<Integer> itSet = set.iterator();
		while (itSet.hasNext())
		{
			Integer i = itSet.next();
			System.out.println(i);
		}


		Map<Integer, String> map = new HashMap<Integer, String>();
		map.put(1, "qwe");
		map.put(2, "asd");
		map.put(3, "zxc");
		Set<Integer> keys		= map.keySet();
		Iterator<Integer> it	= keys.iterator();
		while (it.hasNext())
		{
			Integer key		= it.next();
			System.out.println(key +"___"+ map.get(key));
		}


		Set<Entry<Integer, String>>	entryKey			= map.entrySet();
		Iterator<Entry<Integer, String>> entryIterator	= entryKey.iterator();
		while (entryIterator.hasNext())
		{
			Entry<Integer, String> pair = entryIterator.next();
			System.out.println(pair.getKey() +"___"+ pair.getValue());
		}
	}
}