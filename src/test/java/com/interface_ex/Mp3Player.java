package com.interface_ex;

public interface Mp3Player {
	public void playSong();
	public void stopSong();
}