package com.interface_ex;

public class AndroidPhone extends SmartPhone {
	@Override
	public String getOS() {
		return "Android";
	}

	public static void main(String[] Args)
	{
		System.out.println("=== androidPhone ===");
		AndroidPhone androidPhone = new AndroidPhone();
		androidPhone.playSong();
		androidPhone.stopSong();
		androidPhone.sendCall();
		androidPhone.receiveCall();
		System.out.println("OS: "+ androidPhone.getOS() );

		System.out.println("=== smartPhone ===");
		//SmartPhone s = new SmartPhone();	//NOT ALLOWED BECAUSE IT IS ABSTRACT
		SmartPhone smartPhone = new AndroidPhone();
		smartPhone.playSong();
		smartPhone.stopSong();
		smartPhone.sendCall();
		smartPhone.receiveCall();
		System.out.println("OS: "+ smartPhone.getOS() );

		System.out.println("=== phone ===");
		Phone phone = new AndroidPhone();
		phone.sendCall();
		phone.receiveCall();

		System.out.println("=== mp3Player ===");
		Mp3Player mp3Player = new AndroidPhone();
		mp3Player.playSong();
		mp3Player.stopSong();
	}
}