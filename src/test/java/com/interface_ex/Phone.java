package com.interface_ex;

public interface Phone {
	public void sendCall();
	public void sendCall(int phoneNumber);
	public void receiveCall();
}
