package com.interface_ex;

import java.util.Scanner;

public abstract class SmartPhone implements Phone, Mp3Player {
	private static int phoneNumber=0;
	
	public abstract String getOS();

	@Override
	public void playSong() {
		System.out.println("playSong");
	}

	@Override
	public void stopSong() {
		System.out.println("stopSong");
	}

	@Override
	public void sendCall() {
		System.out.println("sendCall. Enter phone number: ");
		Scanner reader = new Scanner(System.in);
		phoneNumber = reader.nextInt();
		if (reader!=null)
			reader.close();

		sendCall(phoneNumber);
	}

	@Override
	public void sendCall(int phoneNumber) {
		if (phoneNumber != 0)
			System.out.println("sendCall:"+ phoneNumber);
		else
			System.out.println("sendCall: NO PHONE NUMBER");
	}
	
	@Override
	public void receiveCall() {
		System.out.println("receiveCall");
	}
}
