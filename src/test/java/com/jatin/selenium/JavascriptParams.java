package com.jatin.selenium;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class JavascriptParams {

	@Test
	public void testPassingParameters() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();		
		driver.get("https://www.facebook.com");
		Util.wait(2);
		
		WebElement email = driver.findElement(By.id("email"));
		WebElement password = driver.findElement(By.id("pass"));
		
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].value = 'bhatin.shah@gmail.com'; arguments[1].value = 'password'", email, password);
		
		
		Util.wait(60);
		driver.quit();
	}
}
