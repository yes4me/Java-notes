package com.jatin.selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class SelectableTests {
	@Test
	@Ignore
	public void testMoveByOffsetAndClickOnCurrentLocation() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/Selectable.html");

	    WebElement seven = driver.findElement(By.name("seven"));
	    System.out.println("X coordinate: "+seven.getLocation().getX()+" Y coordinate: "+seven.getLocation().getY());
	    Util.wait(3);
	    Actions builder = new Actions(driver);
	    builder.moveByOffset(seven.getLocation().getX()+1, seven.getLocation().getY()+1).click();
	    builder.perform();
		Util.wait(3);
		driver.quit();
	}

	@Test
	@Ignore
	public void testMoveByOffsetMultipleSelect() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/Selectable.html");
	    Util.wait(3);

	    WebElement one = driver.findElement(By.name("one"));
	    WebElement three = driver.findElement(By.name("three"));
	    WebElement six = driver.findElement(By.name("six"));

	    Actions builder = new Actions(driver);

	    // Click on one
	    builder.moveByOffset(one.getLocation().getX() + 1, one.getLocation().getY() + 1).click();
	    builder.perform();

	    Util.wait(3);

	    // Click on three
	    builder.moveByOffset(three.getLocation().getX() + 1, three.getLocation().getY() + 1).click();
	    builder.perform();

	    Util.wait(2);
	    driver.quit();

	}

	@Test
	public void testClickElementOneAfterAnother() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/Selectable.html");
	    Util.wait(3);

	    WebElement one = driver.findElement(By.name("one"));
	    WebElement three = driver.findElement(By.name("three"));
	    WebElement six = driver.findElement(By.name("six"));

	    Actions builder = new Actions(driver);

	    builder.click(one).pause(1000).click(three).pause(1000).click(six);
	    builder.perform();

	    driver.quit();
	}

	@Test
	@Ignore
	public void testMoveByOffsetSelectTogether() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/Selectable.html");
	    Util.wait(3);

	    WebElement one = driver.findElement(By.name("one"));
	    WebElement three = driver.findElement(By.name("three"));
	    WebElement six = driver.findElement(By.name("six"));

	    Actions builder = new Actions(driver);

	    builder.keyDown(Keys.CONTROL).click(one).click(three).click(six).keyUp(Keys.CONTROL);
	    builder.perform();

	    Util.wait(2);
	    driver.quit();

	}
}
