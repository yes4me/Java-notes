package com.jatin.selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FileUploadTest {

	@Test
	@Ignore
	public void testFileUpload() {
		//For HTML uploader
		WebDriver driver = new FirefoxDriver();
		driver.get("https://encodable.com/uploaddemo/");
		Util.wait(5);

		WebElement fileUpload = driver.findElement(By.name("uploadname1"));
		fileUpload.sendKeys("/Users/jatins/Downloads/ami.properties");
		WebElement uploadButton = driver.findElement(By.id("uploadbutton"));
		uploadButton.click();
		
		Util.wait(10);
		driver.quit();
	}
	
	@Test
	@Ignore
	public void testFineUpload() {
		//For Ajax uploader
		WebDriver driver = new FirefoxDriver();
		driver.get("http://fineuploader.com/demos#gallery-view");
		Util.wait(5);

		WebElement fileUpload = driver.findElement(By.name("qqfile"));
		fileUpload.sendKeys("/Users/jatins/Downloads/123me.jpg");

		Util.wait(10);
		driver.quit();
	}
	
}
