package com.jatin.selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDropTest {

	@Test
	public void testDragAndDropBy() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/DragMe.html");
	    Util.wait(3);

	    WebElement draggable = driver.findElement(By.id("draggable"));
	    Actions builder = new Actions(driver);
	    builder.dragAndDropBy(draggable, 100, 100).perform();

	    Util.wait(3);
	    driver.quit();
	}

	@Test
	@Ignore
	public void testDragAndDrop() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/DragAndDrop.html");
	    Util.wait(3);

	    WebElement src = driver.findElement(By.id("draggable"));
	    WebElement target = driver.findElement(By.id("droppable"));

	    Actions builder = new Actions(driver);
	    builder.dragAndDrop(src, target).perform();

	    Util.wait(3);
	    driver.quit();
	}
}