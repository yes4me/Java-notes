package com.jatin.selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ContextClickTest {
	
	@Test
	@Ignore
	public void testContextClick() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();		
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/ContextClick.html");
	    Util.wait(3);
	    
	    WebElement contextMenu = driver.findElement(By.id("div-context"));
	    Actions builder = new Actions(driver);
	    builder.contextClick(contextMenu).perform();
	    

	    Util.wait(2);
	    driver.quit();
	}
	
	@Test
	@Ignore
	public void testContextClickAndAct() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();		
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/ContextClick.html");
	    Util.wait(3);
	    
	    WebElement contextMenu = driver.findElement(By.id("div-context"));
	    Actions builder = new Actions(driver);
	    builder.contextClick(contextMenu).click(driver.findElement(By.name("Item 4"))).perform();;
	    
	    Util.wait(2);
	    driver.quit();
	}
	
	@Test
	public void testContextClickAndActAndHandleAlert() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();		
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/ContextClick.html");
	    Util.wait(3);
	    
	    WebElement contextMenu = driver.findElement(By.id("div-context"));
	    Actions builder = new Actions(driver);
	    builder.contextClick(contextMenu).click(driver.findElement(By.name("Item 4"))).perform();
	    
	    Alert alert = driver.switchTo().alert();
	    alert.accept();
	    
	    Util.wait(2);
	    driver.quit();		
	}
	
}
