package com.jatin.selenium;

import java.io.File;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SwitchWindowTest {
	
	@Test
	public void testWindowSwitch() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file://" + System.getProperty("user.dir") + File.separator + "html" + File.separator + "base.html");
		Util.wait(2);
		
		String basehandle = driver.getWindowHandle();
		System.out.println(basehandle);
		
		//After clicking on the link, a new window is open
		WebElement link = driver.findElement(By.tagName("a"));
		link.click();
		
		for(String handle : driver.getWindowHandles()) {
			if(!handle.equals(basehandle)) {
				driver.switchTo().window(handle);
				break;
			}
		}
		
		// switch back to the original handle
		driver.switchTo().window(basehandle);
		
		

		Util.wait(2);
		driver.quit();
	}

}
