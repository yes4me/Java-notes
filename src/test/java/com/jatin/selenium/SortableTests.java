package com.jatin.selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class SortableTests {

	@Test
	@Ignore
	public void clickAndHoldCurrentPosition() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();		
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/Sortable.html");
	    
	    Util.wait(3);
	    Actions builder = new Actions(driver);
	    builder.moveByOffset(200, 20).clickAndHold().moveByOffset(30, 0);
	    builder.perform();
	    
	    
	    Util.wait(3);
	    driver.quit();
	}
	
	@Test
	@Ignore
	public void clickAndHoldElement() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();		
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/Sortable.html");
	    
	    Util.wait(3);
	    
	    WebElement three = driver.findElement(By.name("three"));
	    
	    Actions builder = new Actions(driver);
	    builder.clickAndHold(three).moveByOffset(30, 30);
	    builder.perform();
	    
	    
	    Util.wait(3);
	    driver.quit();		
	}
	
	@Test
	@Ignore
	public void releaseCurrentLocation() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();		
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/Sortable.html");
	    
	    Util.wait(3);
	    
	    WebElement three = driver.findElement(By.name("three"));
	    
	    Actions builder = new Actions(driver);
	    builder
	    	.clickAndHold(three)
	    	.moveByOffset(20, 0).pause(1000)
	    	.moveByOffset(20, 0).pause(1000)
	    	.moveByOffset(20, 0).pause(1000)
	    	.moveByOffset(20, 0).pause(1000)
	    	.moveByOffset(20, 0).pause(1000)
	    	.moveByOffset(20, 0).pause(1000)	    	
	    	.release();
	    builder.perform();
	    
	    
	    Util.wait(3);
	    driver.quit();		
	}
	
	@Test
	public void releaseOnElement() {
		System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		WebDriver driver = new ChromeDriver();		
	    driver.get("file:///Users/jatins/Documents/ucsc/lecture4/Sortable.html");
	    
	    Util.wait(3);
	    
	    WebElement two = driver.findElement(By.name("two"));
	    WebElement three = driver.findElement(By.name("three"));
	    
	    Actions builder = new Actions(driver);
	    builder.clickAndHold(three).release(two);
	    builder.perform();
	    

	    Util.wait(3);
	    driver.quit();		
	}
	
	
}
