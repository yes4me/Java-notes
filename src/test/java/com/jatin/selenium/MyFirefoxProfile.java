package com.jatin.selenium;

import java.io.File;
import java.io.File;
import java.io.IOException;
import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class MyFirefoxProfile {
	private WebDriver driver;

	@Test
	public void keepingFirefoxProfile() throws IOException
	{
		//SOURCE: http://www.toolsqa.com/selenium-webdriver/custom-firefox-profile/
		//Location of profile: C:\Users\Thomas\AppData\Roaming\Mozilla\Firefox\Profiles
		File profilelocation= new File("C:\\Users\\Thomas\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\i8tmbbh0.default-1436255699940");
		FirefoxProfile profile = new FirefoxProfile(profilelocation);
		WebDriver driver = new FirefoxDriver(profile);
		driver.get("https://www.mint.com");
	}
}