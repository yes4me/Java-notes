package com.jatin.selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class DoubleClickTest {

	@Test
	@Ignore
	public void doubleClickCurrentLocation() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file:///Users/jatins/Documents/ucsc/lecture4/DoubleClick.html");
		Util.wait(2);

		WebElement button = driver.findElement(By.name("dblClick"));
		Actions actionBuilder = new Actions(driver).moveToElement(button).doubleClick();

		Action action = actionBuilder.build();
		action.perform();

		//Alert alert = driver.switchTo().alert();
		//alert.accept();


		Util.wait(10);
		driver.quit();
	}

	@Test
	public void doubleClickElement() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file:///Users/jatins/Documents/ucsc/lecture4/DoubleClick.html");
		Util.wait(2);

		WebElement button = driver.findElement(By.name("dblClick"));
		Actions actionBuilder = new Actions(driver).doubleClick(button);

		Action action = actionBuilder.build();
		action.perform();

		//Alert alert = driver.switchTo().alert();
		//alert.accept();


		Util.wait(10);
		driver.quit();
	}

}
