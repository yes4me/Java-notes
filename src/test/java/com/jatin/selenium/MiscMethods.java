package com.jatin.selenium;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MiscMethods {
	
	private static WebDriver driver;
	
	@BeforeClass
	public static void before() {
		driver = new FirefoxDriver();
	}
	
	@AfterClass
	public static void after() {
		driver.quit();
	}
	
	@Test
	@Ignore
	public void testGetCssValueTest() {
		driver.get("file:///Users/jatins/Documents/ucsc/lecture4/getcss.html");
		Util.wait(1);
		
		WebElement heading = driver.findElement(By.tagName("h1"));
		
		System.out.println("Border = " + heading.getCssValue("border"));
		System.out.println("Border Style = " + heading.getCssValue("border-style"));
		
		System.out.println(heading.getCssValue("border-bottom-width"));
		System.out.println(heading.getCssValue("border-bottom-color"));
		System.out.println(heading.getCssValue("border-bottom-style"));
		System.out.println(heading.getCssValue("color"));
		System.out.println(heading.getCssValue("background-color"));
		
	}
	
	@Test
	@Ignore
	public void getLocationTest() {
		driver.get("file:///Users/jatins/Documents/ucsc/lecture4/getlocation.html");
		Util.wait(1);
		
		WebElement regular = driver.findElement(By.id("standard"));
		WebElement absolutelyPositioned = driver.findElement(By.id("absolute-pos"));
		WebElement fixedPositioned = driver.findElement(By.id("fixed-pos"));
		
		System.out.println("Regular =" + regular.getLocation());
		System.out.println("Absolute = " + absolutelyPositioned.getLocation());
		System.out.println("FIXED = " + fixedPositioned.getLocation());
		
		((JavascriptExecutor)driver).executeScript("window.scrollBy(0, 50)");
		
		System.out.println("Regular =" + regular.getLocation());
		System.out.println("Absolute = " + absolutelyPositioned.getLocation());
		System.out.println("FIXED = " + fixedPositioned.getLocation());
	}
	
	@Test
	@Ignore
	public void getSizeTest() {
		driver.get("file:///Users/jatins/Documents/ucsc/lecture4/getlocation.html");
		Util.wait(1);
		
		WebElement regular = driver.findElement(By.id("standard"));
		WebElement absolutelyPositioned = driver.findElement(By.id("absolute-pos"));
		WebElement fixedPositioned = driver.findElement(By.id("fixed-pos"));
		
		System.out.println(regular.getSize());
		System.out.println(absolutelyPositioned.getSize());
		System.out.println(fixedPositioned.getSize());
	}
	
	@Test
	@Ignore
	public void isDisplayed() {
		driver.get("file:///Users/jatins/Documents/ucsc/lecture4/displayed.html");
		Util.wait(1);
		
		WebElement heading = driver.findElement(By.tagName("h1"));
		System.out.println(heading.isDisplayed());
		heading.click();
	}
	
	@Test
	@Ignore
	public void isEnabledTest() {
		driver.get("file:///Users/jatins/Documents/ucsc/lecture4/isenabled.html");
		Util.wait(1);
		
		WebElement box = driver.findElement(By.id("textbox"));
		System.out.println(box.isEnabled());
		box.sendKeys("something");
		
		Util.wait(3);
		
	}
	
	

}
