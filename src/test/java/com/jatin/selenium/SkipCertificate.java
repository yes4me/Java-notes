/*
TO MAKE A NEW FIREFOX PROFILE:
	CMD>
	cd "C:\Program Files (x86)\Mozilla Firefox\firefox.exe"
	firefox -ProfileManager -no-remote
TO ACCESS THE CURRENT PROFILE:
	FROM THE BROWSER: go to "about:config"
	FROM THE FILE: C:\Users\Thomas\AppData\Roaming\Mozilla\Firefox\profiles.ini
*/

package com.jatin.selenium;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SkipCertificate {
	private WebDriver driver;

	@Before
	public void setUp() throws Exception {
		/*
		//To remove the certificates in Firefox
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.startup.homepage", "http://www.cnn.com");		//start the browser with this website
		profile.setAcceptUntrustedCertificates(true);
		//profile.setAssumeUntrustedCertificateIssuer(true);
		driver = new FirefoxDriver(profile);
		*/
		//driver = new FirefoxDriver();

		/*
		//To remove the certificates in Chrome
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"));
		driver = new ChromeDriver(capabilities);
		*/
		System.setProperty("webdriver.chrome.driver", ".\\vendor\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@After
	public void tearDown() throws Exception {
		System.out.println("DONE");
		driver.quit();
	}


	@Test
	public void testExamples() {
		System.out.println("testExamples");
		driver.get("http://www.google.com");
		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys("100");
	}
}