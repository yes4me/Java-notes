package com.jatin.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Betasoft {
	private WebDriver driver;
	//@FindBy is "executed" before the @BeforeClass so it won't work
	//DOES NOT WORK: @FindBy(id = "covaddr") private WebElement cityName_text;

	@Before
	public void setUp() throws Exception {
		//driver = new FirefoxDriver();

		//System.setProperty("webdriver.chrome.driver", "/Users/jatins/Downloads/chromedriver");
		//work in Eclipse, crash on Gradle: System.setProperty("webdriver.chrome.driver", ".//vendor//chromedriver.exe");
		//work in Eclipse, crash on Gradle: System.setProperty("webdriver.chrome.driver", ".\\vendor\\chromedriver.exe");
		//work in Eclipse, crash on Gradle: System.setProperty("webdriver.chrome.driver", "C:\\save\\thomas\\job\\computer\\Java\\ucsc-quickstart\\vendor\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", ".//vendor//chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	@After
	public void tearDown() throws Exception {
		System.out.println("DONE");
		//driver.quit();
	}


	//Basic test
	@Ignore
	@Test
	public void mint() {
		System.out.println("RUNNING TEST: mint");
		driver.get("http://www.mint.com");
		driver.findElement(By.linkText("Log In")).click();
		driver.findElement(By.id("edit-username")).sendKeys("praveen@gmail.com");
		driver.findElement(By.id("edit-password")).sendKeys("openpassword");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String errormsg=driver.findElement(By.className("errorMessage")).getText();
		assertEquals("abcd", errormsg);
	}

	//Enter the zip code inside an iframe, and check the city name that is return from Ajax
	@Ignore
	@Test
	public void credoZipCode() {
		System.out.println("RUNNING TEST: credoZipCode");
		driver.get("http://www.credomobile.com/coverage");

		driver.findElement( By.className("close") ).click();

		//driver.switchTo().frame(0);		//in xpath, the index of iframe start at 1
		driver.switchTo().frame(driver.findElement(By.xpath("//div[@id='coverageMap']/div/iframe")));
		driver.findElement(By.id("mapzip")).sendKeys("95035");
		driver.findElement(By.id("mapit")).click();

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOf( driver.findElement(By.xpath(".//*[@id='covaddr']")) ));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String cityName = driver.findElement(By.id("covaddr")).getText();

		assertEquals(cityName, "Milpitas, CA 95035");
		assertTrue( cityName.contains("Milpitas") );
		Assert.assertEquals(cityName, "Milpitas, CA 95035");
		Assert.assertTrue( cityName.contains("Milpitas") );
	}

	//After you google "12345", check if the google map refer to the address 12345
	@Ignore
	@Test
	public void google_12345()
	{
		System.out.println("RUNNING TEST: google_12345");
		driver.get("http://www.google.com");

		driver.findElement( By.name("q") ).sendKeys("12345");
		driver.findElement( By.name("btnK") ).submit();
		List<WebElement> maps = driver.findElements( By.id("lu_map") );
		assertEquals(maps.size(), 1);
		//Verify.verify( maps.size()==1 );

		String href = maps.get(0).getAttribute("src");
		System.out.println("href="+ href);
		assertTrue( href.contains("www.google.com/maps") );
	}

	@Test
	public void testExamples() {
		System.out.println("testExamples");
		driver.get("http://www.google.com");

		WebElement element = driver.findElement(By.name("q"));
		element.sendKeys("100");
		driver.findElement( By.name("btnK") ).submit();
	}
}