package com.jatin.selenium;

import java.io.File;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExplicitWait {

	@Test
	@Ignore
	public void willFail() {		
		WebDriver driver = new FirefoxDriver();
		driver.get("file://" + System.getProperty("user.dir") + File.separator + "html" + File.separator + "add-dom.html");
		Util.wait(2);
		
		WebElement firstLink = driver.findElement(By.id("always-visible-link"));
		firstLink.click();
		
		//Util.wait(5);
		
		WebElement secondLink = driver.findElement(By.id("added-link"));
		secondLink.click();
		
		Util.wait(5);
		driver.quit();
	}
	
	@Test
	@Ignore
	public void waitForElement() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file://" + System.getProperty("user.dir") + File.separator + "html" + File.separator + "add-dom.html");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		WebElement firstLink = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("always-visible-link")));
		firstLink.click();
		
		WebElement secondLink = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("added-link")));
		secondLink.click();
		
		Util.wait(5);
		driver.quit();
	}
	
	@Test
	@Ignore
	public void verify() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file://" + System.getProperty("user.dir") + File.separator + "html" + File.separator + "add-dom.html");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		WebElement firstLink = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("always-visible-link")));
		firstLink.click();
		
		WebElement secondLink = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("added-link")));
		secondLink.click();
		
		ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
			//On the fly object. The custom ExpectedCondition can be declared as:
				// ExpectedCondition<Boolean> and return false or true
				// ExpectedCondition<Object> and return null or an object
			
			@Override
			public Boolean apply(WebDriver input) {
				WebElement body = input.findElement(By.tagName("body"));
				String color = body.getCssValue("background-color");
				System.out.println(System.currentTimeMillis() + " : " + color);
				return color.equals("rgba(255, 255, 0, 1)");
			}
		};
		wait.until(condition);
		
		driver.quit();		
	}
	
	@Test
	@Ignore
	public void waitForAlert() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file://" + System.getProperty("user.dir") + File.separator + "html" + 
				File.separator + "delayed-alert.html");
		
		WebElement link = (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.tagName("a")));
		link.click();
		
		Alert alert = (new WebDriverWait(driver, 5)).until(ExpectedConditions.alertIsPresent());
		alert.accept();
		
		Util.wait(2);
		driver.quit();
	}
	
	@Test
	@Ignore
	public void waitForElementToHide() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file://" + System.getProperty("user.dir") + File.separator + "html" + 
				File.separator + "hide-element.html");

		WebElement link = (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.tagName("a")));
		link.click();
		
		Boolean status = (new WebDriverWait(driver, 10)).until(ExpectedConditions.invisibilityOfElementLocated(By.tagName("div")));
		System.out.println("Status: " + status);

		
		Util.wait(2);
		driver.quit();		
	}
	
	@Test
	@Ignore
	public void navigateBetweenPages() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file://" + System.getProperty("user.dir") + File.separator + "html" + 
				File.separator + "page1.html");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		// Get the link
		WebElement link = wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("a")));
		link.click();
		
		// Wait for the title to change
		wait.until(ExpectedConditions.titleIs("Articl"));
		

		Util.wait(2);
		driver.quit();
	}
	
	@Test
	@Ignore
	public void testInfiniteScroll() {
		WebDriver driver = new FirefoxDriver();
		driver.get("file://" + System.getProperty("user.dir") + File.separator + "html" + 
				File.separator + "scroll.html");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		List<WebElement> articles = wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("article")));
		final int numArticles = articles.size();
		System.out.println("Currently Loaded Articles == " + numArticles);
		
		// We want to wait until we load 15 more articles
		ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
			
			@Override
			public Boolean apply(WebDriver d) {
				int currentlyLoadedArticles = d.findElements(By.className("article")).size();
				int increase = currentlyLoadedArticles - numArticles;
				System.out.println(currentlyLoadedArticles + " : (" + (increase) + ")");
				
				// If the increase is less than 15 ... we will scroll to the end of the page
				if(increase < 15) {
					((JavascriptExecutor)d).executeScript("window.scrollTo(0, document.body.offsetHeight)");
					return false;
				} else {
					return true;
				}
			}
		};
		
		wait.until(condition);
		
		driver.quit();
		
	}
	
}
