//http://www.javaworld.com/article/2073352/core-java/simply-singleton.html

package com.static_singleton_ex;

public class Singleton {
	//This is the lazy implementation. The Singleton will be initialized only when needed
	private static Singleton instance = null;
	public int i;		//it is ALWAYS static because instance is static

	// A private Constructor prevents any other class from instantiating.
	private Singleton(){ }


	/* Static 'instance' method */
	public static Singleton getInstance() {
		if(instance == null) {
			instance = new Singleton();
		}
		return instance;
	}

	/* Other methods protected by singleton-ness */
	protected static void demoMethod( ) {
		System.out.println("demoMethod for singleton");
	}
}