package com.static_singleton_ex;

public class DemoSingleton {
	public static void main(String[] args) {
		//Singleton tmp = new Singleton( );	//Cannot call constructor
		Singleton tmp = Singleton.getInstance( );
		tmp.demoMethod( );

		System.out.println(tmp.i);
		tmp.i = 10;
		System.out.println(tmp.i);

		Singleton tmp2 = Singleton.getInstance( );
		System.out.println(tmp2.i);
	}
}