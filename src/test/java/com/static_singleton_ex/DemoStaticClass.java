//http://examples.javacodegeeks.com/java-basics/java-static-class-example/

package com.static_singleton_ex;

public class DemoStaticClass {

	//This is a static class (has only static methods)
	public static class NestedClassStatic {
	    public void aMethod() {
	        System.out.println("NestedClass Static");
	    }
	}
	//inner (non-static nested) class
	public class NestedClassNonStatic {
	    public void aMethod() {
	        System.out.println("NestedClass Non Static");
	    }
	}

	public static void main(String[] args) {
		//nested static class doesn't need instantiation of the outer class
		NestedClassStatic s = new NestedClassStatic();
		s.aMethod();

		//To call non static class, you need a self object
		DemoStaticClass self = new DemoStaticClass();
		DemoStaticClass.NestedClassNonStatic x = self.new NestedClassNonStatic();
		x.aMethod();
	}
}