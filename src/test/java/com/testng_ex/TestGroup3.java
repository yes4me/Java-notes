/* ===========================================================================
Created: 2015/08/23 Thomas Nguyen - thomas_ejob@hotmail.com
Purpose: Reminder for TestNG
=========================================================================== */

package com.testng_ex;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

public class TestGroup3 {

	@BeforeGroups("group3")
	public void beforeGroups() {
		System.out.println("TestGroup3 > @beforeGroups('group3')");
	}
	@AfterGroups("group3")
	public void afterGroups() {
		System.out.println("TestGroup3 > @afterGroups('group3')");
	}


	@Test(groups= "group3")
	public void test11() {
		System.out.println("TestGroup3 > group3.1");
	}
	@Test(groups= "group3")
	public void test12() {
		System.out.println("TestGroup3 > group3.2");
	}

	@Test(groups= "group4")
	public void test21() {
		System.out.println("TestGroup3 > group4.1");
	}
	@Test(groups= "group4")
	public void test22() {
		System.out.println("TestGroup3 > group4.2");
	}

	@Test(dependsOnGroups = {"group3","group4"})
	public void runFinal() {
		System.out.println("TestGroup3 > group3 & group4");
	}
}