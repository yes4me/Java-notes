/* ===========================================================================
Created: 2015/08/22 Thomas Nguyen - thomas_ejob@hotmail.com
Purpose: Reminder for TestNG
Installation on Eclipse: help > install new software > http://beust.com/eclipse
=========================================================================== */

package com.testng_ex;

import static org.testng.Assert.fail;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class TestAnnotation {
	/* ===========================================================================
	Annotations
	=========================================================================== */

	@BeforeClass
	public static void beforeClass()
	{
		System.out.println("This is @BeforeClass");
	}
	@AfterClass
	public static void afterClass()
	{
		System.out.println("This is @AfterClass");
	}
	@BeforeMethod
	public void beforeMethod()
	{
		System.out.println("This is @BeforeMethod");
	}
	@AfterMethod
	public void afterMethod()
	{
		System.out.println("This is @AfterMethod");
	}

	/* ===========================================================================
	TESTS
	=========================================================================== */

	@DataProvider(name="mydata1")
	public Integer[][] myData1()
	{
		return new Integer[][]
		{
			{0, 0,0},
			{4, 2, 6},
			{6, -2, 4}
		};
	}
	@Test(dataProvider="mydata1")					//Cannot add more than 1 dataProvider. Will run 3 times
	public void testAdd(int a, int b, int c) {
		System.out.println("mydata1 running: "+ a +"___"+ b +"___"+ c);
	}

	@Test(threadPoolSize=3, invocationCount=6)	//run a total of 6 times
	public void testThread() {
		System.out.println("testThread: " + Thread.currentThread().getId());
	}

	@Test(groups= "selenium-test", timeOut = 1000, expectedExceptions={ArithmeticException.class,NullPointerException.class})
	public void testException()
	{
		System.out.println("testException running");
		int i = 4/0;
	}

	@Test(enabled=false)
	public void testSkip()
	{
		System.out.println("testSkip NOT running");
	}

	@Test
	public void testFail() {
		//If you want to fail, just change the value
		if (1 == 1)
			System.out.println("testFail running");
		else
			fail("want to fail - Expected exception didn't occur");
	}

	@Test
	public void softAssert() {
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertTrue(false);		//don't throw an exception but record the failure
		softAssert.assertEquals(1, 2);
		//softAssert.assertAll();			//assertAll()} will cause an exception to be thrown if at * least one assertion failed.
	}
}